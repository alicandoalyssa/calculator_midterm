<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Calculator</title>
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" >
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    </head>
    <body style="background-color: LimeGreen;">
          <div class="container text-center">
              <div class="row">
                  <div class="col-md-4 m-auto">
                    <form action="calculation" method="POST">
                        @csrf
                        <div class="card" style="background-color: Darkgreen; margin-top: 100px;">
                          <div class="card-body m-auto">
                              <h2 class="text-center text-light" style="margin: 5px; margin-bottom: 40px;">Midterm</h2>

                              <div class="form-group row">
                                  <div class="col-md-12">
                                      <select name="formula" class="form-control"  style="background-color: YellowGreen; color: black;">
                                          <option>CHOOSE A FORMULA</option>
                                          <option value="1">Perimeter of a Rectangle</option>
                                          <option value="2">The Volume of Cuboid</option>
                                          <option value="3">Area of Square</option>
                                      </select>
                                  </div>
                              </div>

                              <div class="form-group row">
                                  <div class="col-md-12">
                                      <input type="number" name="length" class="form-control" style="background-color: LightGreen; color: black;" placeholder="Enter length">
                                  </div>
                              </div>

                               <div class="form-group row">
                                  <div class="col-md-12">
                                      <input type="number" name="width" class="form-control" style="background-color: LightGreen; color: black;"  placeholder="Enter width">
                                  </div>
                               </div>

                               <div class="form-group row">
                                  <div class="col-md-12">
                                      <input type="number" name="height" class="form-control" style="background-color: LightGreen; color: black;" placeholder="Enter height">
                                  </div>
                               </div>
                    
                          </div>
                          <div class="container" style="background-color: DarkGreen; margin-bottom: 30px;">
                            <input type="submit" name="btn" class="btn btn-primary btn-lg font-weight-bold" style="background-color: YellowGreen; color: black;" value="Enter">
                          </div>
                      </div>
                    </form>
                  </div>
              </div>
          </div> 
          <div class="container" style="margin-top: 30px; background-color: Green;">
              <div class="col-md-4 m-auto">
                  @if(session('message'))
                  <div class="alert alert-warning" style="color: black; background-color: ForestGreen; border-color: Lime;">
                      <h1 class="text-center" style="background-color: ForestGreen;">{{ session('message') }}</h1>
                  </div>
                  @endif
              </div>
          </div>
    </body>
</html>
